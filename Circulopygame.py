from pygame.locals import*  
import pygame 

 
pygame.init() 
screen= pygame.display.set_mode((500,500))

running= True 
movimientocir_x=200 
movimientocir_y=200
rad=150 
while running: 
     
    pygame.time.delay(10)
    #Paso 1: Verificar el evento generado por el usuario 

    for event in pygame.event.get():
        if event.type==pygame.QUIT:
            running=False 
        
        keys = pygame.key.get_pressed() 
        if event.type == pygame.KEYDOWN: 
            if event.key == pygame.K_a: 
                movimientocir_x =  movimientocir_x -60 
            if event.key == pygame.K_d: 
                movimientocir_x =  movimientocir_x +60 
            if event.key == pygame.K_w: 
                movimientocir_y =  movimientocir_y -60 
            if event.key == pygame.K_s: 
                movimientocir_y = movimientocir_y +60 
                               
    #Paso 2: Definir la configuración básica que tendrá la pantalla 
    screen.fill((255,255,255))
    
    #Paso 3: Agregar los elementos de la pantalla 
    
    pygame.draw.circle(screen, (0,150,0), (movimientocir_x, movimientocir_y), rad)
    pygame.display.update()
    
pygame.quit()
    
    #Cambio de coordenadas para las respectivas teclas presionadas: 
    #Tecla flecha izquierda (a): Disminución en la coordenada x 
    #Tecla flecha derecha (d): Incremento en la coordenada x 
    #Tecla flecha hacia arriba (w): Disminución en la coordenada y 
    #Tecla flecha hacia abajo (s): Incremento en la coordenada y  
    
    